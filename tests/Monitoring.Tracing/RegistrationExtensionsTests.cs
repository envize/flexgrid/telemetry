using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OpenTelemetry.Trace;
using Xunit;

[assembly: CollectionBehavior(DisableTestParallelization = true)]

namespace Envize.FlexGrid.Monitoring.Tracing
{
    public class RegistrationExtensions : IDisposable
    {
        #region AddTelemetry_Empty

        [Fact]
        public void AddTelemetry_Empty_ShouldPass_WhenConfigurationIsSet()
        {
            // arrange
            var configuration = new List<KeyValuePair<string, string>>();

            var services = new ServiceCollection();
            services.AddSingleton<IConfiguration>(provider =>
                new ConfigurationBuilder()
                    .AddInMemoryCollection(configuration)
                    .Build());

            // act
            services.AddTracing();

            // assert
            Assert.NotNull(TelemetryContainer.ActivitySource);
        }

        #endregion

        #region AddTelemetry_Configuration

        [Fact]
        public void AddTelemetry_Configuration_ShouldRunConfigurationAction_WhenConfigurationIsValid()
        {
            // arrange
            var services = new ServiceCollection()
                .AddSingleton<IConfiguration>(new ConfigurationBuilder().Build());
            
            var hasRun = false;
            Action<TracerProviderBuilder> configureAction = _ => hasRun = true;

            // act
            services.AddTracing(configureAction);

            // assert
            Assert.True(hasRun);
        }

        #endregion

        #region helper methods

        public void Dispose()
        {
            TelemetryContainer.ActivitySource = null;
        }

        #endregion
    }
}
