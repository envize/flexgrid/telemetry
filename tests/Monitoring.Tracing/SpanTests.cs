using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xunit;

namespace Envize.FlexGrid.Monitoring.Tracing 
{
    public class SpanTests : IDisposable
    {
        private readonly string _name = "test";
        private readonly string _tagKey = "key";
        private readonly string _tagValue = "value";

        #region Constructor

        [Fact]
        public void Constructor_ShouldCreateActivity_WhenListenedTo()
        {
            // arrange
            SetupActivitySource();

            // act
            var sut = new MockSpan(_name);

            // assert
            Assert.NotNull(sut.Activity);
        }

        [Fact]
        public void Constructor_ShouldNotCreateActivity_WhenUnlistened()
        {
            // arrange

            // act
            var sut = new MockSpan(_name);

            // assert
            Assert.Null(sut.Activity);
        }

        #endregion

        #region Dispose

        [Fact]
        public void Dispose_ShouldDisposeActivity_WhenListenedTo()
        {
            // arrange
            SetupActivitySource();
            var sut = new MockSpan(_name);

            // act
            sut.Dispose();

            // assert
            Assert.Null(sut.Activity);
        }

        [Fact]
        public void Constructor_ShouldNotThrowError_WhenUnlistened()
        {
            // arrange
            var sut = new MockSpan(_name);

            // act
            sut.Dispose();

            // assert
            Assert.Null(sut.Activity);
        }

        #endregion

        #region Start

        [Fact]
        public void Start_ShouldSetStartTime_WhenListenedTo()
        {
            // arrange
            SetupActivitySource();
            var sut = new MockSpan(_name);

            // act
            sut.Start();

            // assert
            Assert.NotEqual(default(DateTime), sut.Activity.StartTimeUtc);
        }

        [Fact]
        public void Start_ShouldNotThrowError_WhenUnlistened()
        {
            // arrange
            var sut = new MockSpan(_name);

            // act
            sut.Start();

            // assert
            Assert.Null(sut.Activity);
        }

        #endregion

        #region Stop

        [Fact]
        public void Stop_ShouldSetDuration_WhenListenedTo()
        {
            // arrange
            SetupActivitySource();
            var sut = new MockSpan(_name);
            sut.Start();

            // act
            sut.Stop();

            // assert
            Assert.NotEqual(default(TimeSpan), sut.Activity.Duration);
        }

        [Fact]
        public void Stop_ShouldNotThrowError_WhenUnlistened()
        {
            // arrange
            var sut = new MockSpan(_name);
            sut.Start();

            // act
            sut.Stop();

            // assert
            Assert.Null(sut.Activity);
        }

        #endregion

        #region SetTag

        [Fact]
        public void SetTag_ShouldAddTag_WhenListenedTo()
        {
            // arrange
            SetupActivitySource();
            var sut = new MockSpan(_name);

            // act
            sut.SetTag(_tagKey, _tagValue);

            // assert
            Assert.NotEmpty(sut.Activity.Tags);
        }

        [Fact]
        public void SetTag_ShouldNotThrowError_WhenUnlistened()
        {
            // arrange
            var sut = new MockSpan(_name);

            // act
            sut.SetTag(_tagKey, _tagValue);

            // assert
            Assert.Null(sut.Activity);
        }

        #endregion

        #region AddEvent

        [Fact]
        public void AddEvent_ShouldAddEvent_WhenListenedTo()
        {
            // arrange
            SetupActivitySource();
            var sut = new MockSpan(_name);

            // act
            sut.AddEvent(_name);

            // assert
            Assert.NotEmpty(sut.Activity.Events);
        }

        [Fact]
        public void AddEvent_ShouldAddEventAndTags_WhenListenedTo()
        {
            // arrange
            SetupActivitySource();
            var sut = new MockSpan(_name);

            var tags = new[]
            {
                new KeyValuePair<string, object>(_tagKey, _tagValue)
            };

            // act
            sut.AddEvent(_name, tags);

            // assert
            Assert.NotEmpty(sut.Activity.Events);
        }

        [Fact]
        public void AddEvent_ShouldNotThrowError_WhenUnlistened()
        {
            // arrange
            var sut = new MockSpan(_name);

            // act
            sut.AddEvent(_name);

            // assert
            Assert.Null(sut.Activity);
        }

        #endregion

        #region BuildEvent

        [Fact]
        public void BuildEvent_ShouldReturnEventBuilder_WhenCalled()
        {
            // arrange
            var sut = new MockSpan(_name);

            // act
            var result = sut.BuildEvent(_name);

            // assert
            Assert.NotNull(result);
        }

        #endregion

        #region helper methods

        private void SetupActivitySource()
        {
            TelemetryContainer.ActivitySource = new ActivitySource("test");

            if (!TelemetryContainer.ActivitySource.HasListeners())
            {
                var listener = new ActivityListener
                {
                    ShouldListenTo = _ => true,
                    Sample = (ref ActivityCreationOptions<ActivityContext> _) => ActivitySamplingResult.AllData
                };

                ActivitySource.AddActivityListener(listener);
            }
        }

        public void Dispose()
        {
            TelemetryContainer.ActivitySource = null;
        }

        #endregion
    }

    #region helper classes

    public class MockSpan : Span
    {
        public Activity Activity => _activity;

        public MockSpan(string name) : base(name) {}
    }

    #endregion
}
