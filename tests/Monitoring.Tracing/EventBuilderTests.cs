using System.Collections.Generic;
using Xunit;

namespace Envize.FlexGrid.Monitoring.Tracing
{
    public class EventBuilderTests
    {
        private readonly string _name = "test";
        
        #region SetName

        [Fact]
        public void SetName_ShouldSetName_WhenCalled()
        {
            // arrange
            var sut = new MockEventBuilder(CreateSpan(), _name);
            var expected = "newName";

            // act
            sut.SetName(expected);

            // assert
            Assert.Equal(expected, sut.Name);
        }

        #endregion
        
        #region AddTag

        [Fact]
        public void AddTag_ShouldAddTag_WhenCalled()
        {
            // arrange
            var sut = new MockEventBuilder(CreateSpan(), _name);

            // act
            sut.AddTag("key", "value");

            // assert
            Assert.NotEmpty(sut.Tags);
        }

        #endregion
        
        #region AddTags

        [Fact]
        public void AddTags_ShouldAddTags_WhenNotEmpty()
        {
            // arrange
            var sut = new MockEventBuilder(CreateSpan(), _name);
            var tags = new[]
            {
                new KeyValuePair<string, object>("key", "value")
            };

            // act
            sut.AddTags(tags);

            // assert
            Assert.NotEmpty(sut.Tags);
        }

        [Fact]
        public void AddTags_ShouldNotAddTags_WhenEmpty()
        {
            // arrange
            var sut = new MockEventBuilder(CreateSpan(), _name);
            var tags = new KeyValuePair<string, object>[0];

            // act
            sut.AddTags(tags);

            // assert
            Assert.Empty(sut.Tags);
        }

        #endregion

        #region Build

        [Fact]
        public void Build_ShouldNotThrowException_WhenUnlisted()
        {
            // arrange
            var sut = new MockEventBuilder(CreateSpan(), _name);

            // act
            sut.Build();

            // assert
        }

        #endregion

        #region helper methods

        public MockSpan CreateSpan()
        {
            return new MockSpan(_name);
        }

        #endregion

        #region helper classes

        private class MockEventBuilder : EventBuilder
        {
            public string Name => _name;
            public IList<KeyValuePair<string, object>> Tags => _tags;

            internal MockEventBuilder(Span context, string name) : base(context, name) {}
        }

        #endregion
    }
}
