using System;
using System.Collections.Generic;
using System.Diagnostics;
using OpenTelemetry.Trace;

namespace Envize.FlexGrid.Monitoring.Tracing
{
    public class Span : IDisposable
    {
        protected Activity? _activity;

        public Span(string name)
        {
            _activity = TelemetryContainer.ActivitySource?.StartActivity(name);
        }

        internal Span(string name, ActivityKind activityKind)
        {
            _activity = TelemetryContainer.ActivitySource?.StartActivity(name, activityKind);
        }

        internal Span(string name, ActivityKind activityKind, CorrelationId? correlationId)
        {
            ActivityContext context = default;

            if (correlationId is not null)
            {
                context = new ActivityContext(correlationId.Value.TraceId, correlationId.Value.SpanId,
                    ActivityTraceFlags.None);
            }

            _activity = TelemetryContainer.ActivitySource?.StartActivity(name, activityKind, context);
        }

        public void Start()
        {
            _activity?.Start();
        }

        public void Stop()
        {
            _activity?.Stop();
        }

        public void SetTag(string key, object? value)
        {
            _activity?.AddTag(key, value);
        }

        // public void MarkSuccessful()
        // {
        //     _activity?.SetStatus(Status.Ok);
        // }

        public void SetError(string? description = null)
        {
            _activity?.SetStatus(Status.Error);

            if (!string.IsNullOrEmpty(description))
            {
                _activity?.SetTag("otel.status_description", description);
            }
        }

        public void AddEvent(string name, IEnumerable<KeyValuePair<string, object?>>? tags = null)
        {
            if (_activity != null)
            {
                ActivityTagsCollection? tagCollection = null;

                if (tags != null)
                {
                    tagCollection = new ActivityTagsCollection(tags);
                }

                var @event = new ActivityEvent(name, tags: tagCollection);

                _activity.AddEvent(@event);
            }
        }

        public EventBuilder BuildEvent(string name)
        {
            return new EventBuilder(this, name);
        }

        public void Dispose()
        {
            _activity?.Dispose();
            _activity = null;
        }
    }
}
