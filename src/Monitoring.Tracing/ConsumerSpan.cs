using System.Diagnostics;

namespace Envize.FlexGrid.Monitoring.Tracing
{
    public class ConsumerSpan : Span
    {
        public ConsumerSpan(string name, CorrelationId? correlationId)
            : base(name, ActivityKind.Consumer, correlationId)
        {
        }
    }
}
