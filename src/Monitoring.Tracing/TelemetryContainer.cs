using System.Diagnostics;

namespace Envize.FlexGrid.Monitoring.Tracing
{
    public static class TelemetryContainer
    {
        public static ActivitySource ActivitySource { get; internal set; }
    }
}
