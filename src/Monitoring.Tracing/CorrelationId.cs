using System.Diagnostics;

namespace Envize.FlexGrid.Monitoring.Tracing
{
    public readonly struct CorrelationId
    {
        private const char _separator = '.';

        public ActivityTraceId TraceId { get; init; }
        public ActivitySpanId SpanId { get; init; }

        public CorrelationId(ActivityTraceId traceId, ActivitySpanId spanId)
        {
            TraceId = traceId;
            SpanId = spanId;
        }

        public CorrelationId(string correlationId)
        {
            var parts = correlationId.Split(_separator, 2);
            
            TraceId = ActivityTraceId.CreateFromString(parts[0]);
            SpanId = ActivitySpanId.CreateFromString(parts[1]);
        }

        public override string ToString()
        {
            return $"{TraceId}{_separator}{SpanId}";
        }

        public static implicit operator string(CorrelationId correlationId) => correlationId.ToString();
        public static implicit operator CorrelationId(string correlationId) => new(correlationId);
    }
}
