using System.Collections.Generic;

namespace Envize.FlexGrid.Monitoring.Tracing
{
    public class EventBuilder
    {
        protected readonly Span _context;
        protected string _name;
        protected IList<KeyValuePair<string, object?>> _tags = new List<KeyValuePair<string, object?>>();

        internal EventBuilder(Span context, string name)
        {
            _context = context;
            _name = name;
        }

        public EventBuilder SetName(string name)
        {
            _name = name;

            return this;
        }

        public EventBuilder AddTag(string key, object? value)
        {
            var pair = new KeyValuePair<string, object?>(key, value);
            _tags.Add(pair);

            return this;
        }

        public EventBuilder AddTags(IEnumerable<KeyValuePair<string, object?>>? tags)
        {
            if (tags != null)
            {
                foreach (var tag in tags)
                {
                    AddTag(tag.Key, tag.Value);
                }
            }
            
            return this;
        }

        public Span Build()
        {
            _context.AddEvent(_name, _tags);
            return _context;
        }
    }
}
