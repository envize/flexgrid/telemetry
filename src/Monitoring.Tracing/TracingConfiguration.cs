namespace Envize.FlexGrid.Monitoring.Tracing
{
    public class TracingConfiguration
    {
        public const string SectionName = "Monitoring:Tracing";
        public string? Version { get; set; }
        public string? AgentHost { get; set; }
        public int AgentPort { get; set; } = 6831;
    }
}
