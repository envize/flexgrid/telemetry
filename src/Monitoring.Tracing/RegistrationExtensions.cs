using System;
using System.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using OpenTelemetry.Resources;
using OpenTelemetry.Trace;

namespace Envize.FlexGrid.Monitoring.Tracing
{
    public static class RegistrationExtensions
    {
        public static IServiceCollection AddTracing(
            this IServiceCollection services,
            Action<TracerProviderBuilder>? configure = null
        )
        {
            var provider = services.BuildServiceProvider();
            var configuration = provider.GetRequiredService<IConfiguration>();

            return AddTracing(services, configuration, configure);
        }

        public static IServiceCollection AddTracing(
            this IServiceCollection services,
            IConfiguration configuration,
            Action<TracerProviderBuilder>? configure = default
        )
        {
            var tracingConfiguration = new TracingConfiguration();
            configuration.Bind(TracingConfiguration.SectionName, tracingConfiguration);

            var serviceName = MachineInfo.ApplicationName;
            var activitySource = new ActivitySource(serviceName, tracingConfiguration.Version);
            TelemetryContainer.ActivitySource = activitySource;

            services.AddOpenTelemetryTracing(builder =>
            {
                try
                {
                    builder
                        .SetSampler(new AlwaysOnSampler()) // TODO: change this to a variable one
                        .AddSource(serviceName)
                        .SetResourceBuilder(ResourceBuilder.CreateDefault().AddService(serviceName))
                        .AddAspNetCoreInstrumentation(options =>
                        {
                            options.Enrich = (activity, _, _) =>
                            {
                                activity.SetTag("application-id", MachineInfo.ApplicationId);
                            };
                        })
                        .AddHttpClientInstrumentation()
                        .AddJaegerExporter(options =>
                        {
                            options.AgentHost = tracingConfiguration.AgentHost;
                            options.AgentPort = tracingConfiguration.AgentPort;
                        });
                }
                catch (Exception e)
                {
                    var provider = services.BuildServiceProvider();
                    provider.GetService<ILogger>()?
                        .LogWarning(e, "cannot start OpenTelemetryTracing");
                }

                configure?.Invoke(builder);
            });

            return services;
        }
    }
}
