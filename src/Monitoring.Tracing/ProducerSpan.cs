using System.Diagnostics;

namespace Envize.FlexGrid.Monitoring.Tracing
{
    public class ProducerSpan : Span
    {
        internal ActivityTraceId? TraceId => _activity?.TraceId;
        internal ActivitySpanId? SpanId => _activity?.SpanId;

        private bool _isEvaluated = false;
        private CorrelationId? _correlationId;

        public CorrelationId? CorrelationId
        {
            get
            {
                if (_isEvaluated)
                {
                    return _correlationId;
                }

                if (TraceId.HasValue &&
                    SpanId.HasValue)
                {
                    _correlationId = new CorrelationId(TraceId.Value, SpanId.Value);
                }

                _isEvaluated = true;

                return _correlationId;
            }
        }

        public ProducerSpan(string name) : base(name, ActivityKind.Producer)
        {
        }
    }
}
