﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Enrichers.Span;
using Serilog.Sinks.Elasticsearch;

namespace Envize.FlexGrid.Monitoring.Logging
{
    public static class RegistrationExtensions
    {
        public static IHostBuilder AddLogging(this IHostBuilder builder, IConfiguration configuration)
        {
            var loggingConfiguration = new LoggingConfiguration();
            configuration.Bind(LoggingConfiguration.SectionName, loggingConfiguration);

            if (string.IsNullOrEmpty(loggingConfiguration.Host))
            {
                return builder;
            }

            var environment = MachineInfo.EnvironmentName;

            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .Enrich.WithMachineName()
                .Enrich.WithSpan()
                .WriteTo.Debug()
                .WriteTo.Console()
                .WriteTo.Elasticsearch(GetElasticOptions(loggingConfiguration))
                .Enrich.WithProperty("Environment", environment)
                .MinimumLevel.Information()
                .CreateLogger();

            return builder.UseSerilog();
        }

        private static ElasticsearchSinkOptions GetElasticOptions(LoggingConfiguration configuration)
        {
            var parts = new[]
            {
                MachineInfo.ApplicationName,
                MachineInfo.EnvironmentName.ToLower(),
                MachineInfo.MachineName,
                DateTime.UtcNow.ToString("yyyyMM")
            }.Select(x => x.Replace('.', '-'));

            var index = string.Join('-', parts);

            return new(new Uri(configuration.Host!))
            {
                AutoRegisterTemplate = true,
                IndexFormat = index
            };
        }
    }
}
