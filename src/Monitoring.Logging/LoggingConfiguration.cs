namespace Envize.FlexGrid.Monitoring.Logging
{
    public class LoggingConfiguration
    {
        public const string SectionName = "Monitoring:Logging";
        public string? Host { get; set; }
    }
}
